package com.svenss.coppeltest.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by miguelleon on 02,marzo,2022
 */
@HiltAndroidApp
class App : Application()