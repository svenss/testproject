package com.svenss.coppeltest.di

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.svenss.coppeltest.api.ApiRemote
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by miguelleon on 22,febrero,2022
 */
@Module
@InstallIn(SingletonComponent::class)
class ApiModule {

    companion object{
        private const val URL_BASE = "https://www.superheroapi.com/api/"
        private val rxAdapter = CoroutineCallAdapterFactory()
        private const val readTimeOut = 30L
        private const val connectTimeOut = 30L
    }

    /**
     * Call for HttpClient
     */
    @Provides
    fun getUnSafeOkHttpClient(): OkHttpClient{
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.HEADERS
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val builder = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .connectTimeout(connectTimeOut, TimeUnit.SECONDS)
            .readTimeout(readTimeOut, TimeUnit.SECONDS)

        return builder.build()
    }

    /**
     * Gson Provider
     * */
    @Provides
    internal fun provideGson(): Gson{
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
        return gsonBuilder.create()
    }

    /**
     * Retrofit Provider
     * */
    @Provides
    internal fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit{
        return Retrofit.Builder()
            .baseUrl(URL_BASE)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(rxAdapter)
            .build()
    }

    /** Api Call */
    @Provides
    internal fun provideApi(retrofit: Retrofit): ApiRemote{
        return retrofit.create(ApiRemote::class.java)
    }

}