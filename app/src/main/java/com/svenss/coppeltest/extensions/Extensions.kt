package com.svenss.coppeltest.extensions

import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.svenss.coppeltest.R

/**
 * Created by miguelleon on 22,febrero,2022
 */

fun TextView.setListName(position: Int){
    val text = "${this.context.getString(R.string.super_hero)} ${position.plus(1)}"
    this.text = text
}

fun ImageView.loadImage(uri: String?){
    val progress = CircularProgressDrawable(this.context).apply {
        strokeWidth = 5f
        centerRadius = 30f
        start()
    }

    val options = RequestOptions()
        .placeholder(progress)
        .error(R.drawable.copp)

    Glide.with(this.context)
        .setDefaultRequestOptions(options)
        .load(uri)
        .into(this)
}

fun TextView.setDetail(code: Int, detail: String){
    val resIdText = getResId(code)
    val text = "${this.context.getString(resIdText)} $detail"

    this.text = text
}

fun FragmentManager.makeFragment(fragment: Fragment, tag: String){
    this.beginTransaction().apply {
        replace(R.id.rl_container_fragment_home, fragment, tag)
        commit()
    }
}

fun ProgressBar.setVisibilityLoad(state: Boolean){
    this.visibility = if (state) View.VISIBLE else View.GONE
}

private fun getResId(code: Int): Int {
    return when(code){
        0 -> R.string.intelligence
        1 -> R.string.strength
        2 -> R.string.speed
        3 -> R.string.durability
        4 -> R.string.combat
        5 -> R.string.full_name
        6 -> R.string.alter_egos
        7 -> R.string.aliases
        8 -> R.string.place_of_birth
        9 -> R.string.first_appareance
        10 -> R.string.publisher
        11 -> R.string.bad
        12 -> R.string.full_name_63
        13 -> R.string.race
        14 -> R.string.height
        15 -> R.string.weight
        16 -> R.string.eye_color
        17 -> R.string.hair_color
        18 -> R.string.occupation
        19 -> R.string.base
        20 -> R.string.group_affiliations
        else -> R.string.relatives
    }
}
