package com.svenss.coppeltest.modules.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.svenss.coppeltest.adapter.SearchAdapter
import com.svenss.coppeltest.databinding.FragmentSearchBinding
import com.svenss.coppeltest.extensions.setVisibilityLoad
import com.svenss.coppeltest.models.HeroModelSearch
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

/**
 * Created by miguelleon on 23,febrero,2022
 */
@AndroidEntryPoint
class SearchFragment: Fragment(), View.OnClickListener{

    private lateinit var binding: FragmentSearchBinding

    private val viewModel by viewModels<SearchViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        observeViewModel()
    }

    private fun initViews(){
        binding.btnSearchFragment.setOnClickListener(this)
    }

    private fun observeViewModel(){
        viewModel.listHero.observe(activity as LifecycleOwner, onSearchUpdate())
    }

    private fun onSearchUpdate() = Observer<HeroModelSearch>{ data ->
        data?.let {
            setVisibilityProgress(false)
            if (it.response == "success"){
                bindRecycler(it)
            }else{
                toastError()
            }
        }
    }

    private fun bindRecycler(it: HeroModelSearch){
        binding.rvSearchFragment.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = SearchAdapter(it)
        }
    }

    private fun toastError(){
        Toast.makeText(context, "Hero not available", Toast.LENGTH_SHORT).show()
    }

    private fun startSearch(){
        val hero = binding.etSearchFragment.text.toString()

        if (hero.isNotEmpty()){
            setVisibilityProgress(true)
            viewModel.searchHero(hero.lowercase(Locale.getDefault()))
        }else{
            toastError()
        }
    }

    private fun setVisibilityProgress(state: Boolean){
        binding.pbSearchFragment.setVisibilityLoad(state)
    }

    override fun onClick(p0: View?) {
        startSearch()
    }
}