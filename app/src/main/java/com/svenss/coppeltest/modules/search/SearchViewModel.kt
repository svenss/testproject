package com.svenss.coppeltest.modules.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.svenss.coppeltest.delegate.ResultResponse
import com.svenss.coppeltest.models.HeroModelSearch
import com.svenss.coppeltest.repository.HeroRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Created by miguelleon on 23,febrero,2022
 */
@HiltViewModel
class SearchViewModel @Inject constructor(private val repository: HeroRepository): ViewModel() {

    /** Live Data **/
    val listHero: MutableLiveData<HeroModelSearch> by lazy { MutableLiveData<HeroModelSearch>() }

    fun searchHero(hero: String) = CoroutineScope(Dispatchers.Main).launch {
        val response = repository.searchHero(hero)

        withContext(Dispatchers.Main){
            listHero.value = when(response){
                is ResultResponse.Success -> response.data
                is ResultResponse.Error -> HeroModelSearch("error")
            }
        }
    }
}