package com.svenss.coppeltest.modules.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.svenss.coppeltest.R
import com.svenss.coppeltest.databinding.ActivityHomeBinding
import com.svenss.coppeltest.extensions.makeFragment
import com.svenss.coppeltest.modules.list.FragmentList
import com.svenss.coppeltest.modules.search.SearchFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViews()
        makeFragment(0)
    }

    private fun initViews(){
        navigationMenu()
    }

    private fun navigationMenu(){
        binding.btnNavigationMenuHome.setOnItemSelectedListener {
            when(it.itemId){
                R.id.mnu_first -> makeFragment(0)
                else -> makeFragment(1)
            }
            return@setOnItemSelectedListener true
        }
    }

    private fun makeFragment(code: Int){
        val fragment = if (code == 0) FragmentList() else SearchFragment()
        val tag = if (code == 0) "list" else "search"
        this.supportFragmentManager.makeFragment(fragment, tag)
    }
}