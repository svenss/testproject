package com.svenss.coppeltest.modules.detail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.svenss.coppeltest.databinding.ActivityDetailBinding
import com.svenss.coppeltest.extensions.loadImage
import com.svenss.coppeltest.extensions.setDetail
import com.svenss.coppeltest.models.HeroModel

/**
 * Created by miguelleon on 22,febrero,2022
 */
class DetailActivity: AppCompatActivity(){

    private lateinit var binding: ActivityDetailBinding
    private var data = HeroModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        data = Gson().fromJson(this.intent.getStringExtra("data"), HeroModel::class.java)
        binding.svDetailFragment.loadImage(data.image?.url)
        bindItems()
    }


    private fun bindItems(){
        setPowerStats()
        setBiography()
        setAppearance()
        setWork()
        setConnections()
    }

    private fun setPowerStats(){
        data.powerstats?.let {
            with(binding){
                tvIntelligenceDetailFragment.setDetail(0, it.intelligence)
                tvStrengthDetailFragment.setDetail(1 , it.strength)
                tvSpeedDetailFragment.setDetail(2, it.speed)
                tvDurabilityDetailFragment.setDetail(3, it.durability)
            }
        }
    }

    private fun setBiography(){
        data.biography?.let {
            with(binding){
                tvFullNameFragment.setDetail(5, it.fullName)
                tvAlterNameFragment.setDetail(6, it.alterEgos)
                tvAliasesNameFragment.setDetail(7, it.aliases[0])
                tvBirthFragment.setDetail(8, it.placeOfBirth)
                tvFirstNameFragment.setDetail(9, it.firstAppearance)
                tvPublisherFragment.setDetail(10, it.publisher)
                tvAligmentNameFragment.setDetail(11, it.alignment)
            }
        }
    }

    private fun setAppearance(){
        data.appearance?.let {
            with(binding){
                tvGenderNameFragment.setDetail(12, it.gender)
                tvRaceNameFragment.setDetail(13, it.race)
                tvHeightNameFragment.setDetail(14, it.height[0])
                tvWeightFragment.setDetail(15, it.weight[0])
                tvEyeNameFragment.setDetail(16, it.eyeColor)
                tvHairFragment.setDetail(17, it.hairColor)
            }
        }
    }

    private fun setWork(){
        data.work?.let {
            with(binding){
                tvOccupationNameFragment.setDetail(18, it.occupation)
                tvBaseNameFragment.setDetail(19, it.base)
            }
        }
    }

    private fun setConnections(){
        data.connections?.let {
            with(binding){
                tvGroupAffiliationsNameFragment.setDetail(20, it.groupAffiliation)
                tvRelativesNameFragment.setDetail(21, it.relatives)
            }
        }
    }
}