package com.svenss.coppeltest.modules.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.svenss.coppeltest.adapter.ListHeroAdapter
import com.svenss.coppeltest.databinding.FragmentListBinding

/**
 * Created by miguelleon on 23,febrero,2022
 */
class FragmentList: Fragment() {

    private lateinit var binding: FragmentListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
    }

    private fun initViews(){
        startRecyclerList()
    }

    private fun startRecyclerList(){
        binding.rvListHome.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = ListHeroAdapter()
        }
    }
}