package com.svenss.coppeltest.modules.hero

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.svenss.coppeltest.databinding.ActivityHeroBinding
import com.svenss.coppeltest.models.HeroModel
import com.svenss.coppeltest.extensions.loadImage
import com.svenss.coppeltest.modules.detail.DetailActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HeroActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHeroBinding
    private val viewModel by viewModels<HeroViewModel>()

    private var dataHero = HeroModel()
    private var id = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHeroBinding.inflate(layoutInflater)
        setContentView(binding.root)

        id = intent.getIntExtra("id", 1)

        initViews()
        observeViewModel()
    }

    private fun initViews(){
        viewModel.getHero(id)
        binding.btnDetailsHeroActivity.setOnClickListener { startDetail() }
    }

    private fun observeViewModel(){
        viewModel.dataHero.observe(this, onHeroUpdate())
    }

    private fun onHeroUpdate() = Observer<HeroModel>{ data ->
        dismissProgress()
        data?.let {
            if (it.response == "success"){
                dataHero = it
                bindItems(it)
            }else{
                toastError()
            }
        }
    }

    private fun bindItems(it: HeroModel){
        with(binding){
            ivHeroActivity.loadImage(it.image?.url)
            tvNameHeroActivity.text = it.name
        }
    }

    private fun toastError(){
        Toast.makeText(this, "Hero not available", Toast.LENGTH_SHORT).show()
    }

    private fun dismissProgress(){
        binding.pbHeroActivity.visibility = View.GONE
    }

    private fun startDetail(){
        val intent = Intent(this, DetailActivity::class.java)
        val gson = Gson()
        intent.putExtra("data", gson.toJson(dataHero))
        startActivity(intent)
    }
}