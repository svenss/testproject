package com.svenss.coppeltest.modules.hero

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.svenss.coppeltest.delegate.ResultResponse
import com.svenss.coppeltest.models.HeroModel
import com.svenss.coppeltest.repository.HeroRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Created by miguelleon on 22,febrero,2022
 */
@HiltViewModel
class HeroViewModel @Inject constructor(private val repository: HeroRepository) : ViewModel() {

    /** live data values **/
    val dataHero: MutableLiveData<HeroModel> by lazy { MutableLiveData<HeroModel>() }

    fun getHero(id: Int) = CoroutineScope(Dispatchers.IO).launch {
        val response = repository.getHero(id)

        withContext(Dispatchers.Main){
            dataHero.value = when(response){
                is ResultResponse.Success -> response.data
                is ResultResponse.Error -> HeroModel("error")
            }
        }
    }
}