package com.svenss.coppeltest.repository

import com.svenss.coppeltest.datasource.RemoteDataSource
import javax.inject.Inject

/**
 * Created by miguelleon on 22,febrero,2022
*/
class HeroRepository @Inject constructor(private val remoteDataSource: RemoteDataSource){

    suspend fun getHero(id: Int) = remoteDataSource.getHero(id)

    suspend fun searchHero(hero: String) = remoteDataSource.searchHero(hero)
}