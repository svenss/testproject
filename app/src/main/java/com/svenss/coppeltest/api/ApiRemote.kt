package com.svenss.coppeltest.api

import com.svenss.coppeltest.models.HeroModel
import com.svenss.coppeltest.models.HeroModelSearch
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by miguelleon on 02,marzo,2022
 */
interface ApiRemote {

    @GET("{access-token}/{id}")
    suspend fun getHeroAsync(
        @Path("access-token") token: String,
        @Path("id") id: String
    ): Response<HeroModel>

    @GET("{access-token}/search/{hero}")
    suspend fun searchHeroAsync(
        @Path("access-token") token: String,
        @Path("hero") hero: String
    ): Response<HeroModelSearch>

}