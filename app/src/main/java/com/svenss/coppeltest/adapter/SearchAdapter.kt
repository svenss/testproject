package com.svenss.coppeltest.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.svenss.coppeltest.databinding.AdapterSearchBinding
import com.svenss.coppeltest.extensions.loadImage
import com.svenss.coppeltest.models.HeroModelSearch
import com.svenss.coppeltest.modules.detail.DetailActivity

/**
 * Created by miguelleon on 23,febrero,2022
 */
class SearchAdapter(private val data: HeroModelSearch): RecyclerView.Adapter<SearchAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchAdapter.ViewHolder {
        val binding = AdapterSearchBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount() = data.results?.size?: 0

    override fun onBindViewHolder(holder: SearchAdapter.ViewHolder, position: Int) {
        holder.bindItems()
    }

    /** View Holder to bind items */
    inner class ViewHolder(
        private val binding: AdapterSearchBinding
    ): RecyclerView.ViewHolder(binding.root){

        fun bindItems(){
            val results = data.results?.get(adapterPosition)
            with(binding){
                svSearchAdapter.loadImage(results?.image?.url)
                tvSearchAdapter.text = results?.name
                svSearchAdapter.setOnClickListener { startDetail() }
            }
        }

        private fun startDetail(){
            val intent = Intent(itemView.context, DetailActivity::class.java)
            val data = data.results?.get(adapterPosition)
            val gson = Gson()
            intent.putExtra("data", gson.toJson(data))
            itemView.context.startActivity(intent)
        }
    }
}