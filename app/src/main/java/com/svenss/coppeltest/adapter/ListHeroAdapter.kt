package com.svenss.coppeltest.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.svenss.coppeltest.databinding.AdapterListHeroBinding
import com.svenss.coppeltest.extensions.setListName
import com.svenss.coppeltest.modules.hero.HeroActivity

/**
 * Created by miguelleon on 22,febrero,2022
 */
class ListHeroAdapter: RecyclerView.Adapter<ListHeroAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = AdapterListHeroBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount() = 731

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems()
    }

    /** View Holder to bind items */
    inner class ViewHolder(
        private val binding: AdapterListHeroBinding
        ): RecyclerView.ViewHolder(binding.root){

        fun bindItems(){
            with(binding){
                tvTitleListAdapter.setListName(adapterPosition)
                tvTitleListAdapter.setOnClickListener { startScreenHero() }
            }
        }

        private fun startScreenHero(){
            val intent = Intent(itemView.context, HeroActivity::class.java)
            val position = adapterPosition.plus(1)
            intent.putExtra("id", position)
            itemView.context.startActivity(intent)
        }
    }
}