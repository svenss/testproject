package com.svenss.coppeltest.models

import com.google.gson.annotations.SerializedName


data class HeroModel(
    var response: String? = "",
    var appearance: Appearance? = null,
    var biography: Biography? = null,
    var connections: Connections? = null,
    var id: String? = "",
    var image: Image? = null,
    var name: String? = "",
    var powerstats: Powerstats? = null,
    var work: Work? = null
)

data class Image(
    val url: String
)

data class Connections(
    @SerializedName("group-affiliation") val groupAffiliation: String,
    val relatives: String
)

data class Appearance(
    @SerializedName("eye-color") val eyeColor: String,
    val gender: String,
    @SerializedName("hair-color") val hairColor: String,
    val height: List<String>,
    val race: String,
    val weight: List<String>
)

data class Biography(
    val aliases: List<String>,
    val alignment: String,
    @SerializedName("alter-egos") val alterEgos: String,
    @SerializedName("first-appearance") val firstAppearance: String,
    @SerializedName("full-name") val fullName: String,
    @SerializedName("place-of-birth") val placeOfBirth: String,
    val publisher: String
)

data class Powerstats(
    var combat: String,
    var durability: String,
    var intelligence: String,
    var power: String,
    var speed: String,
    var strength: String
)

data class Work(
    val base: String,
    val occupation: String
)


/** search hero **/
data class HeroModelSearch(
    var response: String? = "",
    var results: MutableList<HeroModel>? = mutableListOf()
)