package com.svenss.coppeltest.datasource

import com.svenss.coppeltest.api.ApiRemote
import com.svenss.coppeltest.delegate.ResultResponse
import com.svenss.coppeltest.delegate.safeApiCall
import javax.inject.Inject


/**
 * Created by miguelleon on 22,febrero,2022
 */
class RemoteDataSource @Inject constructor(private val apiRemote: ApiRemote){

    private val token = "2496364390592143"

    suspend fun getHero(id: Int) = safeApiCall(
        call = {
            val response = apiRemote
                .getHeroAsync(token, id.toString())
            if (response.isSuccessful) {
                return@safeApiCall ResultResponse.Success(response.body())
            }
            return@safeApiCall ResultResponse.Error(0)
        },
        errorMessage = 0
    )

    suspend fun searchHero(hero: String) = safeApiCall(
        call = {
            val response = apiRemote
                .searchHeroAsync(token, hero)
            if (response.isSuccessful) {
                return@safeApiCall ResultResponse.Success(response.body())
            }
            return@safeApiCall ResultResponse.Error(0)
        },
        errorMessage = 0
    )
}